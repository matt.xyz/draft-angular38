export interface IPersona {
  nombre: string;
  direccion: string;
  acepto: any;
}